package com.unibankbootcamp.relations.repository;

import com.unibankbootcamp.relations.entity.entity_one_to_many.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
}

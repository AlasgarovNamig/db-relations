package com.unibankbootcamp.relations.repository;

import com.unibankbootcamp.relations.entity.entity_one_to_one.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address,Long> {
}

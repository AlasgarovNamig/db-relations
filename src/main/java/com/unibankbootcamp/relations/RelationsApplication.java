package com.unibankbootcamp.relations;

import com.unibankbootcamp.relations.entity.entity_one_to_one.Address;
import com.unibankbootcamp.relations.entity.entity_one_to_one.User;
import com.unibankbootcamp.relations.repository.AddressRepository;
import com.unibankbootcamp.relations.repository.FacultyRepository;
import com.unibankbootcamp.relations.repository.StudentRepository;
import com.unibankbootcamp.relations.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;

@SpringBootApplication
public class RelationsApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private FacultyRepository facultyRepository;

	public static void main(String[] args) {
		SpringApplication.run(RelationsApplication.class, args);
	}

	@Override
	@Transactional
	public void run(String... args) throws Exception {
//		System.out.println(userRepository.findAll());
//		System.out.println(addressRepository.findAll());

//		User user  = User.builder()
//				.build();
//		User user1 = userRepository.save(user);
//
//		Address address = Address.builder()
//				.cityName("Sumgait")
//				.user(user1).build();
//		addressRepository.save(address);


//		Address address = Address.builder()
//				.cityName("Sumgait")
//				.build();
//		addressRepository.save(address);
//		User user  = User.builder()
//				.address(address)
//				.build();
//		User user1 = userRepository.save(user);

		System.out.println(studentRepository.findAll());
		System.out.println(facultyRepository.findAll());



	}
}
// adressin icinde useri save

//eksclude edende exclude elediyim terefden
// save eleye bilmirem ve mene iki terefli lazim olsa cixisim yoxdu
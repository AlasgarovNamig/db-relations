package com.unibankbootcamp.relations.entity.entity_many_to_many;

import com.unibankbootcamp.relations.entity.entity_one_to_one.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = Pupil.TABLE_NAME)
public class Pupil {
    public static final String TABLE_NAME = "pupil";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany
    @JoinTable(name = "pupil_course",
            joinColumns = @JoinColumn(name = "pupil_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id"))
    private Set<Course> courses = new HashSet<>();
}

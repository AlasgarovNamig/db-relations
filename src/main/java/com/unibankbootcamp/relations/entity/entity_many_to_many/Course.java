package com.unibankbootcamp.relations.entity.entity_many_to_many;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = Course.TABLE_NAME)
public class Course {
    public static final String TABLE_NAME = "course";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}

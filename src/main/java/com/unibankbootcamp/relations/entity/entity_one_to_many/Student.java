package com.unibankbootcamp.relations.entity.entity_one_to_many;

import com.unibankbootcamp.relations.entity.entity_one_to_one.User;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = Student.TABLE_NAME)
public class Student {
    public static final String TABLE_NAME = "student";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "faculty_id")
    private Faculty faculty;
}

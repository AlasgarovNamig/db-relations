package com.unibankbootcamp.relations.entity.entity_one_to_many;

import com.unibankbootcamp.relations.entity.entity_one_to_one.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = Faculty.TABLE_NAME)
public class Faculty {
    public static final String TABLE_NAME = "faculty";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "faculty", cascade = CascadeType.ALL)
    private List<Student> studentList;
}

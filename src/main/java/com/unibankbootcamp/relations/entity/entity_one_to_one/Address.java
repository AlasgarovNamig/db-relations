package com.unibankbootcamp.relations.entity.entity_one_to_one;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
//@ToString(exclude = "user")
@Table(name = Address.TABLE_NAME)
public class Address {

    public static final String TABLE_NAME = "addres";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String cityName;

    @OneToOne(mappedBy = "address")
    @ToString.Exclude
    private User user;
    
}

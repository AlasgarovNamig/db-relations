package com.unibankbootcamp.relations.service.impl;

import com.unibankbootcamp.relations.dto.UserDto;
import com.unibankbootcamp.relations.entity.entity_one_to_one.User;
import com.unibankbootcamp.relations.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {
    @Override
    public List<User> allUser() {
        return null;
    }

    @Override
    public User getUserById(Long id) {
        return null;
    }

    @Override
    public void createUser(UserDto dto) {

    }

    @Override
    public void updateUser(UserDto dto) {

    }

    @Override
    public void deleteUser(Long id) {

    }
}

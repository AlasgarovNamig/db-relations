package com.unibankbootcamp.relations.service;

import com.unibankbootcamp.relations.dto.UserDto;
import com.unibankbootcamp.relations.entity.entity_one_to_one.User;

import java.util.List;

public interface UserService {
    List<User> allUser();
    User getUserById(Long id);
    void createUser(UserDto dto);
    void updateUser(UserDto dto);
    void deleteUser(Long id);
}
